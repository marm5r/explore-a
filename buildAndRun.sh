#!/bin/sh
mvn clean package && docker build -t marm5r.jee8/jee8-test .
docker rm -f jee8-test || true && docker run -d -p 8080:8080 -p 4848:4848 --name jee8-test marm5r.jee8/jee8-test 
