package marm5r.anordnen.entity;

import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;

@Interceptor
@FooBinding
public class TheInterceptor {

    @AroundInvoke
    public Object logMethodEntry(InvocationContext invocationContext) throws Exception {
        System.out.println("invocationContext = " + invocationContext);
        return invocationContext.proceed();
    }

}
