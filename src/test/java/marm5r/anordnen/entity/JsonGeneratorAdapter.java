package marm5r.anordnen.entity;

import javax.json.JsonValue;
import javax.json.stream.JsonGenerator;
import java.math.BigDecimal;
import java.math.BigInteger;

public class JsonGeneratorAdapter implements JsonGenerator {

    private final JsonGenerator delegate;

    public JsonGeneratorAdapter(JsonGenerator delegate) {
        this.delegate = delegate;
    }

    @Override
    public JsonGenerator writeStartObject() {
        delegate.writeStartObject();
        return this;
    }

    @Override
    public JsonGenerator writeStartObject(String name) {
        delegate.writeStartObject(name);
        return this;
    }

    @Override
    public JsonGenerator writeKey(String name) {
        delegate.writeKey(name);
        return this;
    }

    @Override
    public JsonGenerator writeStartArray() {
        delegate.writeStartArray();
        return this;
    }

    @Override
    public JsonGenerator writeStartArray(String name) {
        delegate.writeStartArray(name);
        return this;
    }

    @Override
    public JsonGenerator write(String name, JsonValue value) {
        delegate.write(name, value);
        return this;
    }

    @Override
    public JsonGenerator write(String name, String value) {
        delegate.write(name, value);
        return this;
    }

    @Override
    public JsonGenerator write(String name, BigInteger value) {
        delegate.write(name, value);
        return this;
    }

    @Override
    public JsonGenerator write(String name, BigDecimal value) {
        delegate.write(name, value);
        return this;
    }

    @Override
    public JsonGenerator write(String name, int value) {
        delegate.write(name, value);
        return this;
    }

    @Override
    public JsonGenerator write(String name, long value) {
        delegate.write(name, value);
        return this;
    }

    @Override
    public JsonGenerator write(String name, double value) {
        delegate.write(name, value);
        return this;
    }

    @Override
    public JsonGenerator write(String name, boolean value) {
        delegate.write(name, value);
        return this;
    }

    @Override
    public JsonGenerator writeNull(String name) {
        delegate.writeNull(name);
        return this;
    }

    @Override
    public JsonGenerator writeEnd() {
        delegate.writeEnd();
        return this;
    }

    @Override
    public JsonGenerator write(JsonValue value) {
        delegate.write(value);
        return this;
    }

    @Override
    public JsonGenerator write(String value) {
        delegate.write(value);
        return this;
    }

    @Override
    public JsonGenerator write(BigDecimal value) {
        delegate.write(value);
        return this;
    }

    @Override
    public JsonGenerator write(BigInteger value) {
        delegate.write(value);
        return this;
    }

    @Override
    public JsonGenerator write(int value) {
        delegate.write(value);
        return this;
    }

    @Override
    public JsonGenerator write(long value) {
        delegate.write(value);
        return this;
    }

    @Override
    public JsonGenerator write(double value) {
        delegate.write(value);
        return this;
    }

    @Override
    public JsonGenerator write(boolean value) {
        delegate.write(value);
        return this;
    }

    @Override
    public JsonGenerator writeNull() {
        delegate.writeNull();
        return this;
    }

    @Override
    public void close() {
        delegate.close();
    }

    @Override
    public void flush() {
        delegate.flush();
    }
}
