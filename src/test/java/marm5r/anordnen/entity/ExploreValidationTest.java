package marm5r.anordnen.entity;

import org.junit.jupiter.api.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import javax.validation.constraints.NotNull;
import java.util.Set;

public class ExploreValidationTest {

    @Test
    void exploreValidation() {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();
        Set<ConstraintViolation<Fahrkosten>> violations = validator.validate(new Fahrkosten());
        System.out.println("violations = " + violations);
    }

    static class Fahrkosten {

        @NotNull
        private String entfernung;

    }

}
