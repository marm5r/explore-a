package marm5r.anordnen.entity;

import javax.enterprise.util.AnnotationLiteral;
import javax.interceptor.InterceptorBinding;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Target({ TYPE, METHOD })
@Retention(RUNTIME)
@InterceptorBinding
public @interface FooBinding {

    @SuppressWarnings("serial")
    final class Literal extends AnnotationLiteral<FooBinding> implements FooBinding {
        public static final Literal INSTANCE = new Literal();
    }
}
