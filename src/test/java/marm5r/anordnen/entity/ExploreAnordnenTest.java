package marm5r.anordnen.entity;

import com.google.common.collect.ImmutableMap;
import marm5r.anordnen.api.AnordnungService;
import marm5r.anordnen.api.Anordnungsdaten;
import marm5r.anordnen.api.FachlichVergleichbar;
import marm5r.anordnen.explore.Fahrkosten;
import marm5r.anordnen.explore.FahrkostenAnordenbarFactory;
import marm5r.anordnen.explore.FahrkostenBase;
import marm5r.anordnen.explore.FahrkostenOpnv;
import marm5r.anordnen.explore.FahrkostenPkw;
import marm5r.anordnen.jpa.JpaAnordnungService;
import marm5r.anordnen.jpa.spi.MetamodelBasierteAnordenbarFactory;
import marm5r.anordnen.jpa.spi.MitJpaAnordenbar;
import marm5r.anordnen.jpa.spi.AnordenbarFactory;
import org.jboss.weld.junit5.WeldInitiator;
import org.jboss.weld.junit5.WeldJunit5Extension;
import org.jboss.weld.junit5.WeldSetup;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import javax.enterprise.inject.Instance;
import javax.enterprise.inject.spi.CDI;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;

@ExtendWith(WeldJunit5Extension.class)
class ExploreAnordnenTest {

    private static final String PERSISTENCE_UNIT_NAME = "defaultPU";

    private static final ImmutableMap<String, String> PERSISTENCE_PROPERTIES = ImmutableMap.of(
            "eclipselink.logging.level.sql", "FINE",
            "javax.persistence.jtaDataSource", "",
            "javax.persistence.schema-generation.database.action", "drop-and-create",
            "javax.persistence.transactionType", "RESOURCE_LOCAL",
            "javax.persistence.jdbc.url", "jdbc:h2:mem:test"
    );

    private static final ImmutableMap<String, String> PERSISTENCE_PROPERTIES_WITH_SCHEMA_GENARATION =
            new ImmutableMap.Builder<String, String>()
                    .put("javax.persistence.schema-generation.scripts.action", "drop-and-create")
                    .put("javax.persistence.schema-generation.scripts.create-target", "target/ddl/create.sql")
                    .put("javax.persistence.schema-generation.scripts.drop-target", "target/ddl/drop.sql")
                    .putAll(PERSISTENCE_PROPERTIES).build();

    private static EntityManagerFactory emf;
    private static EntityManager em;

    @BeforeAll
    static void setup() {
        emf = Persistence.createEntityManagerFactory(
                PERSISTENCE_UNIT_NAME, PERSISTENCE_PROPERTIES_WITH_SCHEMA_GENARATION);
        em = emf.createEntityManager();
    }

    @AfterAll
    static void teardown() {
        emf.close();
    }

    @WeldSetup
    public WeldInitiator weld = WeldInitiator
            .from(
                    AnordnungService.class,
                    JpaAnordnungService.class,
                    AnordenbarFactory.class,
//                    NoOpAnordenbarFactory.class,
//                    FahrkostenAnordenbarFactory.class,
                    MetamodelBasierteAnordenbarFactory.class)
            .setPersistenceContextFactory(ip -> em)
            .setPersistenceUnitFactory(ip -> emf)
            .build();

    @Inject
    AnordnungService anordnungService;

    @Test
    void shouldPersistEntity() {

        Fahrkosten fahrkosten = new Fahrkosten("deadbeef-dead-dead-dead-deaddeafbeef", "bla");
        FahrkostenPkw fahrkostenPkw = new FahrkostenPkw("deadbeef-dead-dead-dead-deaddeafbeef", "bla", "103");
        FahrkostenOpnv fahrkostenOpnv = new FahrkostenOpnv("deadbeef-dead-dead-dead-deaddeafbeef", "bla", "203");

        runInTransaction(em -> em.persist(fahrkosten));
        runInTransaction(em -> em.persist(fahrkostenPkw));
        runInTransaction(em -> em.persist(fahrkostenOpnv));

        runInTransaction(em -> anordnungService.ordneAn(
                FahrkostenBase.class,
                new Anordnungsdaten(1l, LocalDateTime.now(), "deadbeef-dead-dead-dead-deaddeafbeef")));

        runInTransaction(em -> em.createNativeQuery(
                String.format("SCRIPT TO '%s'", Paths.get("target", "snap.sql").toAbsolutePath().toString())).getResultList());

        runInTransaction(em -> {
            AnordenbarFactory factory = CDI.current()
                    .select(AnordenbarFactory.class)
                    .get();
            List<Fahrkosten> list = em.createQuery("select f from Fahrkosten f", Fahrkosten.class)
                    .getResultList();
            list.forEach(e -> {
                MitJpaAnordenbar anordenbar = factory.createAnordenbar(e);
                em.detach(e);
                anordenbar.wirdAlsNeustandGespeichert();
                Object merged = em.merge(e);

            });
        });

    }


    @Test
    void exploreAnordnung() {

    }

    <T> T callInTransaction(Function<EntityManager, T> callback) {
//        EntityManager em = emf.createEntityManager();
        try {
            EntityTransaction tx = em.getTransaction();
            tx.begin();
            try {
                T result = callback.apply(em);
                tx.commit();
                return result;
            } catch (RuntimeException ex) {
                if (tx.isActive()) {
                    tx.rollback();
                }
                throw ex;
            }
        } finally {
            em.clear();
//            em.close();
        }
    }

    void runInTransaction(Consumer<EntityManager> callback) {
        callInTransaction(em -> {
            callback.accept(em);
            return null;
        });
    }

    public static class NoOpAnordenbarFactory implements AnordenbarFactory {

        @Override
        public MitJpaAnordenbar createAnordenbar(Object entity) {
            throw new RuntimeException();
        }

        @Override
        public boolean akzeptiert(Class entityTyp) {
            return false;
        }
    }


}
