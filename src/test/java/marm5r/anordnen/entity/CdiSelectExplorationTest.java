package marm5r.anordnen.entity;

import org.jboss.weld.junit5.auto.AddBeanClasses;
import org.jboss.weld.junit5.auto.AddEnabledInterceptors;
import org.jboss.weld.junit5.auto.EnableAutoWeld;
import org.junit.jupiter.api.Test;

import javax.enterprise.inject.Instance;
import javax.inject.Inject;

@EnableAutoWeld
@AddEnabledInterceptors({TheInterceptor.class})
@AddBeanClasses({CdiSelectExplorationTest.Foo.class, BlaExtractionStrategy.class, BlubExtractionStrategy.class})
public class CdiSelectExplorationTest {

//    @WeldSetup
//    public WeldInitiator weld = WeldInitiator
//            .from(
//                    Foo.class
//            ).build();

    @Inject
    Instance<ExtractionStrategy> instance;

    @Inject
    Foo foo;

    @Test
    void explore() {
        foo.ping();
    }

    @FooBinding
    public static class Foo {
        boolean ping() {
            return true;
        }
    }


}

