package marm5r.anordnen.entity;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.json.bind.Jsonb;
import javax.json.bind.JsonbBuilder;
import javax.json.bind.JsonbConfig;
import javax.json.bind.annotation.JsonbTypeSerializer;
import javax.json.bind.serializer.JsonbSerializer;
import javax.json.bind.serializer.SerializationContext;
import javax.json.stream.JsonGenerator;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ExploreJsonbTest {

    @Test
    void shouldSerializeUnwrapped() {

        Jsonb jsonb = JsonbBuilder.create(new JsonbConfig()
                .withSerializers(new UnwrappedSerializer()));
        String json = jsonb.toJson(new Representation());
        System.out.println("json = " + json);

        Entity entity = jsonb.fromJson(json, Entity.class);
        assertEquals("some data", entity.test);

    }

    @JsonbTypeSerializer(UnwrappedSerializer.class)
    public static class Representation {

        public Entity links = new Entity("link");
        public Entity embedded = new Entity("embedded");

        public Entity entity = new Entity("some data");

    }

    //    @JsonbTypeSerializer(UnwrappedSerializer.class)
    public static class Entity {

        public String test;

        public Entity() {
        }

        public Entity(String test) {
            this.test = test;
        }
    }

    public static class UnwrappedSerializer implements JsonbSerializer<Representation> {

        @Override
        public void serialize(Representation obj, JsonGenerator generator, SerializationContext ctx) {
            System.out.println("serialize");
//            ctx.serialize("bla", obj.entity, generator);
            generator.writeStartObject();
            ctx.serialize("_links", obj.links, generator);
            ctx.serialize(obj.entity, new UnwrappedGenerator(generator));
            ctx.serialize("_embedded", obj.embedded, generator);
            generator.writeEnd();
        }
    }

    static class UnwrappedGenerator extends JsonGeneratorAdapter {

        private int startCounter = 0;

        public UnwrappedGenerator(JsonGenerator delegate) {
            super(delegate);
        }

        @Override
        public JsonGenerator writeStartObject() {
            startCounter++;
            if (startCounter == 1) {
                return this;
            }
            return super.writeStartObject();
        }

        @Override
        public JsonGenerator writeStartObject(String name) {
            startCounter++;
            return super.writeStartObject(name);
        }

        @Override
        public JsonGenerator writeStartArray() {
            startCounter++;
            return super.writeStartArray();
        }

        @Override
        public JsonGenerator writeStartArray(String name) {
            startCounter++;
            return super.writeStartArray(name);
        }

        @Override
        public JsonGenerator writeEnd() {
            startCounter--;
            if (startCounter == 0) {
                return this;
            }
            return super.writeEnd();
        }
    }


}
