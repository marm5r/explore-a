package marm5r;


import javax.json.bind.Jsonb;
import javax.json.bind.JsonbBuilder;
import javax.json.bind.JsonbConfig;
import javax.ws.rs.ext.ContextResolver;
import javax.ws.rs.ext.Provider;

@Provider
public class JsonbProvider implements ContextResolver<Jsonb> {

    @Override
    public Jsonb getContext(Class<?> type) {
        System.out.println("type = " + type);

        JsonbConfig config = new JsonbConfig()
                .withPropertyVisibilityStrategy(new AnordenbarVisibilityStrategy());

        return JsonbBuilder.create(config);
    }
}
