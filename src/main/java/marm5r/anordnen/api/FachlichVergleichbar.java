package marm5r.anordnen.api;

public interface FachlichVergleichbar {

    boolean fachlichGleich(Object other);

}
