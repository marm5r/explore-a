package marm5r.anordnen.api;

public interface AnordnungService {

    void ordneAn(Class entityTyp, Anordnungsdaten anordnungsdaten);

}
