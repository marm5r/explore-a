package marm5r.anordnen.api;

import java.time.LocalDateTime;
import java.util.Objects;

public final class Anordnungsdaten {

    private final Long anordnungId;

    private final LocalDateTime anordnungszeitpunkt;

    private final String fallId;

    public Anordnungsdaten(Long anordnungId, LocalDateTime anordnungszeitpunkt, String fallId) {
        this.anordnungId = anordnungId;
        this.anordnungszeitpunkt = anordnungszeitpunkt;
        this.fallId = fallId;
    }

    public Long anordnungId() {
        return anordnungId;
    }

    public LocalDateTime anordnungszeitpunkt() {
        return anordnungszeitpunkt;
    }

    public String fallId() {
        return fallId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Anordnungsdaten that = (Anordnungsdaten) o;
        return anordnungId.equals(that.anordnungId) &&
                anordnungszeitpunkt.equals(that.anordnungszeitpunkt) &&
                fallId.equals(that.fallId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(anordnungId, anordnungszeitpunkt, fallId);
    }
}
