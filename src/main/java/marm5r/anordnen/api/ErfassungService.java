package marm5r.anordnen.api;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class ErfassungService {

    @PersistenceContext
    EntityManager em;

    public <T> T erfassen(T entity) {
        em.persist(entity);
        return entity;
    }

    public <T> T aendern(T entitiy) {
        return em.merge(entitiy);
    }
}
