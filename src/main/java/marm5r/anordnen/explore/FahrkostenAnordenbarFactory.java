package marm5r.anordnen.explore;

import marm5r.anordnen.jpa.spi.AnordenbarFactory;
import marm5r.anordnen.jpa.spi.EntityIdAccessor;
import marm5r.anordnen.jpa.spi.MitJpaAnordenbar;

public class FahrkostenAnordenbarFactory implements AnordenbarFactory {

    @Override
    public MitJpaAnordenbar createAnordenbar(Object entity) {
        Fahrkosten fahrkosten = (Fahrkosten) entity;
        return MitJpaAnordenbar.decorate(
                fahrkosten,
                fahrkosten.anordenbarSupport,
                new EntityIdAccessor(
                        (e, id) -> fahrkosten.surrogateKey = id,
                        e -> fahrkosten.surrogateKey));
    }

    @Override
    public boolean akzeptiert(Class entityTyp) {
        return Fahrkosten.class.equals(entityTyp);
    }
}
