package marm5r.anordnen.explore;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

@Entity
@Table(name = "FK")
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
//@DiscriminatorValue("B")
public class Fahrkosten extends FahrkostenBase {

    @Column(name = "DATA", nullable = false)
    private String data;

    /**
     * ORM only.
     */
    protected Fahrkosten() {
    }

    public Fahrkosten(String fallId, String data) {
        super(fallId);
        this.data = data;
    }

    @Override
    public boolean fachlichGleich(Object other) {
        return this.data.equals(((Fahrkosten) other).data);
    }
}
