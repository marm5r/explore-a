package marm5r.anordnen.explore;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "FK_PKW")
//@DiscriminatorValue("P")
public class FahrkostenPkw extends Fahrkosten {

    @Column(name = "PKW_KM")
    private String pkwKm;

    protected FahrkostenPkw() {
    }

    public FahrkostenPkw(String fallId, String data, String pkwKm) {
        super(fallId, data);
        this.pkwKm = pkwKm;
    }

    public String getPkwKm() {
        return pkwKm;
    }

    public void setPkwKm(String pkwKm) {
        this.pkwKm = pkwKm;
    }
}
