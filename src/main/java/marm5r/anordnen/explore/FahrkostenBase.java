package marm5r.anordnen.explore;

import marm5r.anordnen.api.FachlichVergleichbar;
import marm5r.anordnen.jpa.spi.AnordenbarSupport;

import javax.persistence.*;

@MappedSuperclass
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
//@DiscriminatorColumn(name = "DTYPE")
public abstract class FahrkostenBase implements FachlichVergleichbar {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "defaultSequence")
    @SequenceGenerator(name = "defaultSequence", sequenceName = "SEQ_DEFAULT")
    @Column(name = "ID")
    Long surrogateKey;

    @Embedded
    AnordenbarSupport anordenbarSupport;

    protected FahrkostenBase() {
    }

    public FahrkostenBase(String fallId) {
        this.anordenbarSupport = new AnordenbarSupport(fallId);
    }
}
