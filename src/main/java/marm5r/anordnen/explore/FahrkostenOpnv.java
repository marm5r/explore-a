package marm5r.anordnen.explore;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "FK_OPNV")
//@DiscriminatorValue("O")
public class FahrkostenOpnv extends Fahrkosten {

    @Column(name = "OPNV_KM")
    private String opnvKm;

    protected FahrkostenOpnv() {
    }

    public FahrkostenOpnv(String fallId, String data, String opnvKm) {
        super(fallId, data);
        this.opnvKm = opnvKm;
    }

    public String getOpnvKm() {
        return opnvKm;
    }

    public void setOpnvKm(String opnvKm) {
        this.opnvKm = opnvKm;
    }
}
