package marm5r.anordnen.boundary;

import marm5r.anordnen.api.AnordnungService;
import marm5r.anordnen.api.ErfassungService;
import marm5r.anordnen.explore.Fahrkosten;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;

@Path("anordnung")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class AnordnungResource {

    private final AtomicInteger counter = new AtomicInteger(0);

    @Inject
    ErfassungService erfassungService;

    @Inject
    AnordnungService anordnungService;

    @GET
    public String test() {
        return "hello";
    }

    @GET
    @Path("update")
    public Fahrkosten post() {
        Fahrkosten fahrkosten = new Fahrkosten(
                UUID.randomUUID().toString(),
                "DATEN_" + counter.incrementAndGet());
        return erfassungService.erfassen(fahrkosten);
    }

    @PUT
    @Path("update")
    public Fahrkosten put(Fahrkosten fahrkosten) {
        return erfassungService.aendern(fahrkosten);
    }

}
