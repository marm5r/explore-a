package marm5r.anordnen.jpa.spi;

import marm5r.anordnen.api.Anordnungsdaten;
import marm5r.anordnen.api.FachlichVergleichbar;

class MitJpaAnordenbarDecorator implements MitJpaAnordenbar {

    private final FachlichVergleichbar entity;

    private final AnordenbarSupport anordenbarSupport;

    private final EntityIdAccessor entityIdAccessor;

    MitJpaAnordenbarDecorator(FachlichVergleichbar entity, AnordenbarSupport anordenbarSupport, EntityIdAccessor entityIdAccessor) {
        this.entity = entity;
        this.anordenbarSupport = anordenbarSupport;
        this.entityIdAccessor = entityIdAccessor;
    }

    @Override
    public void wirdAlsNeustandGespeichert() {
        Long id = entityIdAccessor.getId(entity);
        entityIdAccessor.setId(entity, null);
        anordenbarSupport.neutstand();
        anordenbarSupport.setTraceId(id);
    }

    @Override
    public void wirdAlsUnveraenderlicherStandGespeichert(Anordnungsdaten anordnungsdaten) {
        anordenbarSupport.angeordnetMit(anordnungsdaten);
        anordenbarSupport.setTraceId(entityIdAccessor.getId(entity));
    }

    @Override
    public FachlichVergleichbar unwrap() {
        return entity;
    }
}
