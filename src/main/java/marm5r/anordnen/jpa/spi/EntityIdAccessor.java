package marm5r.anordnen.jpa.spi;

import java.util.function.BiConsumer;
import java.util.function.Function;

public final class EntityIdAccessor {

    private final BiConsumer<Object, Long> setter;

    private final Function<Object, Long> getter;

    public EntityIdAccessor(BiConsumer<Object, Long> setter, Function<Object, Long> getter) {
        this.setter = setter;
        this.getter = getter;
    }

    public void setId(Object entity, Long id) {
        setter.accept(entity, id);
    }

    public Long getId(Object entity) {
        return getter.apply(entity);
    }
}
