package marm5r.anordnen.jpa.spi;

import marm5r.anordnen.api.FachlichVergleichbar;

import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.persistence.metamodel.Attribute;
import javax.persistence.metamodel.EntityType;
import javax.persistence.metamodel.Metamodel;
import javax.persistence.metamodel.SingularAttribute;
import java.lang.reflect.Field;

public class MetamodelBasierteAnordenbarFactory implements AnordenbarFactory {

    @PersistenceUnit
    EntityManagerFactory emf;

    @Override
    public MitJpaAnordenbar createAnordenbar(Object entity) {
        Metamodel metamodel = emf.getMetamodel();

        EntityType<?> entityType = metamodel.entity(entity.getClass());
        SingularAttribute<?, Long> idAttribute = entityType.getId(Long.class);
        Field idField = (Field) idAttribute.getJavaMember();
        idField.setAccessible(true);

        Attribute<?, ?> anordenbarSupportAttribute = entityType.getAttribute("anordenbarSupport");
        Field anordenbarSupportMember = (Field) anordenbarSupportAttribute.getJavaMember();
        anordenbarSupportMember.setAccessible(true);

        try {
            AnordenbarSupport as = (AnordenbarSupport) anordenbarSupportMember.get(entity);
            return MitJpaAnordenbar.decorate(
                    (FachlichVergleichbar) entity,
                    as,
                    entityIdAccessor(idField));


        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }

    }

    @Override
    public boolean akzeptiert(Class entityTyp) {
        return FachlichVergleichbar.class.isAssignableFrom(entityTyp);
    }

    private EntityIdAccessor entityIdAccessor(Field field) {
        return new EntityIdAccessor(
                (entity, id) -> {
                    try {
                        field.set(entity, id);
                    } catch (IllegalAccessException e) {
                        throw new RuntimeException(e);
                    }
                },
                entity -> {
                    try {
                        return (Long)field.get(entity);
                    } catch (IllegalAccessException e) {
                        throw new RuntimeException(e);
                    }
                }
        );
    }

}
