package marm5r.anordnen.jpa.spi;

public interface AnordenbarFactory {

    MitJpaAnordenbar createAnordenbar(Object entity);

    boolean akzeptiert(Class entityTyp);

}
