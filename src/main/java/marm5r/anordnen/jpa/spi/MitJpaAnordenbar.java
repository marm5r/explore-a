package marm5r.anordnen.jpa.spi;

import marm5r.anordnen.api.Anordnungsdaten;
import marm5r.anordnen.api.FachlichVergleichbar;

public interface MitJpaAnordenbar {

    void wirdAlsNeustandGespeichert();

    void wirdAlsUnveraenderlicherStandGespeichert(Anordnungsdaten anordnungsdaten);

    FachlichVergleichbar unwrap();

    static MitJpaAnordenbar decorate(
            FachlichVergleichbar entity,
            AnordenbarSupport anordenbarSupport,
            EntityIdAccessor entityIdAccessor) {
        return new MitJpaAnordenbarDecorator(entity, anordenbarSupport, entityIdAccessor);
    }

}
