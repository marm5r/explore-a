package marm5r.anordnen.jpa.spi;

import marm5r.anordnen.api.Anordnungsdaten;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.time.LocalDateTime;

@Embeddable
public class AnordenbarSupport {

    @Column(name = "ANORDNUNG_ID")
    private Long anordnungId;

    @Column(name = "ANORDNUNG_ZEITPUNKT")
    private LocalDateTime anordnungszeitpunkt;

    @Column(name = "FALL_ID", nullable = false, updatable = false)
    private String fallId;

    @Column(name = "TRACE_ID")
    private Long traceId;


    /**
     * ORM only.
     */
    protected AnordenbarSupport() {
    }

    public AnordenbarSupport(String fallId) {
        this.fallId = fallId;
    }

    public void setTraceId(Long traceId) {
        this.traceId = traceId;
    }

    public void angeordnetMit(Anordnungsdaten anordnungsdaten) {
        this.anordnungId = anordnungsdaten.anordnungId();
        this.anordnungszeitpunkt = anordnungsdaten.anordnungszeitpunkt();
    }

    public void neutstand() {
        this.anordnungId = null;
        this.anordnungszeitpunkt = null;
    }

    public Long getAnordnungId() {
        return anordnungId;
    }

    public LocalDateTime getAnordnungszeitpunkt() {
        return anordnungszeitpunkt;
    }

    public String getFallId() {
        return fallId;
    }

}
