package marm5r.anordnen.jpa;

import marm5r.anordnen.api.AnordnungService;
import marm5r.anordnen.api.Anordnungsdaten;
import marm5r.anordnen.jpa.spi.MitJpaAnordenbar;
import marm5r.anordnen.jpa.spi.AnordenbarFactory;

import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

public class JpaAnordnungService implements AnordnungService {

    @PersistenceContext
    EntityManager em;

    @Inject
    Instance<AnordenbarFactory> anordenbarFactories;

    @Override
    public void ordneAn(Class entityTyp, Anordnungsdaten anordnungsdaten) {
        AnordenbarFactory anordenbarFactory = anordenbarFactories.stream()
                .filter(f -> f.akzeptiert(entityTyp))
                .findFirst()
                .orElseThrow(RuntimeException::new);

        em.createQuery("select e from Fahrkosten e", Object.class)
                .getResultStream()
                .peek(em::detach)
                .map(anordenbarFactory::createAnordenbar)
                .forEach(a -> ordneAn(a, anordnungsdaten));
    }

    private void ordneAn(MitJpaAnordenbar anordenbar, Anordnungsdaten anordnungsdaten) {
        anordenbar.wirdAlsUnveraenderlicherStandGespeichert(anordnungsdaten);
        em.merge(anordenbar.unwrap());

        anordenbar.wirdAlsNeustandGespeichert();
        em.persist(anordenbar.unwrap());

    }

}
